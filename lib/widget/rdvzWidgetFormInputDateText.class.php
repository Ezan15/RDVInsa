<?php

class rdvzWidgetFormInputDateText extends sfWidgetFormInputText
{
  private
    $comment_value ;

  protected function configure($options = array(), $attributes = array())
  {
    parent::configure($options, $attributes) ;
    $this->comment_value = "" ;
  }

  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    $res = parent::render($name, $value, $attributes, $errors) ;
    $res .= ' '.__('Commentaire').' (<em>'.__('facultatif').'</em>) : <input type="text" onchange="checkHour(this)" name="'.str_replace('date','comment',$name).'" value="'.$this->comment_value.'" />' ;
    $res .= '<span class="validHourWrap">';
    $res .= '<img style="display:none" class="validHour" src="/images/valid.png" title="' . __("Format de date reconnu") .'" />';
    $res .= '<img style="display:none" class="invalidHour" src="/images/warning_16.png" title="' . __("Format de date non reconnu") . '"/>';
    $res .= '</span>';
    $res .= ' <img src="/images/info_16.png" title="'.__('Formats conseillés: ').'
'.__('18:37 ou 6:37 PM pour une heure.').'
'.__('18:37-19:36 pour un créneau') . '" alt="'.__('Formats conseillés: ').'
'.__('18:37 ou 6:37 PM pour une heure.').'
'.__('18:37-19:36 pour un créneau') . '" />';

    return $res ;
  }

  public function setCommentValue($com_val = null)
  {
    $this->comment_value = $com_val ;
  }
}
