jQuery.fn.exists = function(){return jQuery(this).length>0;}

$(document).ready(function()
{
  $('#tips a').click(function()
  {
    $('#tips').slideUp() ;
  }); 

  // Select URL when user clicks on input
  $("#calUrlInput").focus(function()
  {
    $(this).select();
  });

  $("#form").find('input:checkbox').change(function()
  {
    $(this).parent().toggleClass('ok') ;
    $(this).parent().toggleClass('poll_td') ;
  }) ;

  $(".my_meetings").children('li').mouseenter(function()
  {
    $('.meeting_name', this).hide() ;
    $('.meeting_code_div', this).show() ;
    $('.actions', this).slideDown('fast') ;
  }) ;

  $(".my_meetings").children('li').mouseleave(function()
  {
    $('.actions', this).slideUp('fast') ;
    $('.meeting_name', this).show() ;
    $('.meeting_code_div', this).hide() ;
  }) ;

// A modifier, ça rame pour le moment.
//  $('form').nextAll('td').click(function () { $('input:checkbox', this).click(); });

  $("#follow_link").click(function()
  {
    img = $(this).children('img') ;
    if(img.hasClass('followed'))
      $(this).html(not_followed_image) ;
    else if(img.hasClass('not_followed'))
      $(this).html(followed_image) ;
  });
}) ;

// Indicate to user if the format is correct (only an indication)
window.checkHour = function (target) {
    var $target = $(target);
    var format = $target.val();
    console.log($target);
    console.log(format);
    if (format.match(/(?:^(\d{1,2})[h:](\d\d)?$)|(?:^(\d{1,2}):(\d\d)?\s*(PM|AM)$)|(?:^([0-9:hPMA]+)\s*(?:à|to|-)\s*([0-9:hPMA]+)$)/i)) {
        console.log($target.siblings(".validHour"));
        $target.siblings(".validHourWrap").children(".validHour").show();
        $target.siblings(".validHourWrap").children(".invalidHour").hide();
    }
    else {
        $target.siblings(".validHourWrap").children(".validHour").hide();
        $target.siblings(".validHourWrap").children(".invalidHour").show();
    }
};

