BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//rdv.insa-rennes/3A PROJECT//FR
CALSCALE:GREGORIAN
<?php 
foreach($meetings as $meeting)
{
    $votes = Doctrine::getTable('meeting_poll')->getByUid($user->getId(),$meeting->getId());
    if(count($votes)!=0)//votes can be empty if user deleted his vote. In that case we should not do anything.
    {
        foreach($votes as $vote)
        {
          $poll=$vote->getPoll();	  
          $meetingDate=$vote->getMeetingDate();
	  // if the meeting is closed, dates are put only if both the user and the final schedules indicated by creator are ok.
	  if($meeting->getClosed())
              $finalSchedule = Doctrine::getTable('meeting_poll')->retrieveFinalScheduleByDateId($meetingDate->getId());
	  if($poll == 1 && ($meeting->getClosed() == 0 || $finalSchedule->getPoll() == 1))
	  { 
	    $date = $meetingDate->getDate();
            $hour = $vote->getMeetingDate()->getComment();
            $parsedHour = calendarActions::parseTime($hour);
            preg_match("/(\d{4})-(\d\d)-(\d\d)/",$date,$matches);
            $date = $matches[1].$matches[2].$matches[3];//suppression des -
            echo "BEGIN:VEVENT\r\n";
            echo "DTSTAMP:".gmdate('Ymd')."T".gmdate('His')."\r\n";
            if ($meeting->getClosed() == 1) {
                echo "STATUS:CONFIRMED\r\n";
                echo "X-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\n";
                echo "X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\r\n";
            }
            else {
                echo "STATUS:TENTATIVE\r\n";
                echo "X-MICROSOFT-CDO-INTENDEDSTATUS:TENTATIVE\r\n";
                echo "X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE\r\n";
            }
            echo "DESCRIPTION:" . preg_replace("(\r\n|\r|\n)", "\\n", $meeting->getDescription()).preg_replace("(\r\n|\r|\n)","\\n","\nhttp://". sfConfig::get('app_url').'/'.$meeting->getHash())."\r\n";
            echo "URL:http://" . sfConfig::get('app_url').url_for('auth/mh?m='.$meeting->getHash())."\r\n";  
            if($parsedHour!=null){
                
                    echo "DTSTART:".$date."T";
                    if($parsedHour['hoursStart']<10) echo "0";
                    echo $parsedHour['hoursStart'];
		    if($parsedHour['minuteStart']<10) echo "0";
		    echo $parsedHour['minutesStart']."00\r\n";

                    if($parsedHour['type']=='slot'){
                        echo "DTEND:".$date."T";
                        if($parsedHour['hoursEnd']<10) echo "0";
                        echo $parsedHour['hoursEnd'];
			if($parsedHour['minutesEnd']<10) echo "0";
			echo $parsedHour['minutesEnd']."00\r\n";
                    }    
                    echo "SUMMARY:".$meeting->getTitle();
            }
            else {
                echo "DTSTART;VALUE=DATE:".$date."\r\n";
                echo "SUMMARY:".$meeting->getTitle().", ".$hour;
            }
            if($meeting->getClosed()!=1) {
                echo " [Tentative]";
            }
		    echo "\r\n";
            echo "UID:".$meeting->getHash().$vote->getId()."@".sfConfig::get('app_url'). "\r\n";
            echo "END:VEVENT\r\n";
	  }
        }
    }
}
?>
END:VCALENDAR

