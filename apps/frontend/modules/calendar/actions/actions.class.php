<?php

/**
 * calendar actions.
 *
 * @package    rdvz
 * @subpackage calendar
 * @author
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class calendarActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }

  /**
   * Gives the calendar file (.ics) corresponding to all the user's repsonses
   * @param sfRequest $request the calendar id.
   */
  public function executeGetICS(sfWebRequest $request)
  {
      $calId = $request->getParameter('h');
      $this->user = Doctrine::getTable('user')->retrieveByCalendarURL($calId);
      $this->meetings = Doctrine::getTable('meeting')->getMeetingsFollowedByUser($this->user->getId());

      // Prepare the response type and the file name.
      $this->getResponse()->setContentType('text/calendar'); 
      $this->getResponse()->setHttpHeader('Content-Disposition', 'attachment; filename=RDVInsa_calendar'.'.ics');
  }

  /**
   * Read dictionary file to convert from keywords (e.g. "noon", "morning", ...)
   * to hours
   *
   * Note: This does not use locale because the poll creator can have a different one
   *        than one of the participant
   *
   * WARNING: Not to be used if file gets larger
   * WARNING: Because it is mutually recursive with parseTime (it should be no more than
   *            three levels deep), be cautious not to put plain words in the dictionary.
   *            Aliases are possible, but not recommended.
   *
   * @param string $word Keyword to be searched
   * @return array see calendarActions::parseTime
   **/
  private function timeFromDictionary($word)
  {
      $content = file_get_contents(__DIR__ . "/dictionary.json");
      $dictionary = json_decode($content, true);

      if (array_key_exists($word, $dictionary)) {
          return calendarActions::parseTime($dictionary[$word]);
      }
      else {
          return null;
      }
  }

  /**
   * Parse text representing an hour or a timeslot
   *
   * @param string $time Time in text format
   * @return array
   *         {
   *            "timeStart" => [the number of seconds from midnight of the same day,  (or the first hour with a timeslot)}
   *            "timeEnd"   => [the second hour in a timeslot, or null]
   *            "type"      => "hour" if an hour, "slot" if a slot
                "hoursStart"
                "minutesStart"
                "hoursEnd"
                "minutesEnd"
   *         }
   */
  public static function parseTime($time)
  {
      $timeStart = null;
      $timeEnd = null;
      $hoursStart = null;
      $minutesStart = null;
      $hoursEnd = null;
      $minutesEnd = null;
      $type = null;
      if (preg_match("/^[a-zA-Z-]+$/", $time))
      {
          return calendarActions::timeFromDictionary($time);
      }
      if (preg_match("/^(\d{1,2})[h:](\d\d)?$/", $time, $matches))
      {
          //  18:37
          //  18h37
          //  18h
          $hoursStart = $matches[1];
          $minutesStart = !empty($matches[2]) ? $matches[2] : "00";
          $timeStart = (60 * $hoursStart + $minutesStart) * 60;
          $type = "hour";
      } else if (preg_match("/^(\d{1,2}):(\d\d)?\s*(PM|AM)$/", $time, $matches))
      {
          $hoursStart = $matches[1] + ($matches[3] == 'PM' ? 720 : 0);
          $minutesStart = !empty($matches[2]) ? $matches[2] : "00";
          $timeStart = (60 * $hoursStart + $minutesStart) * 60;
          $type = "hour";
          //  6:37 PM
      } else if (preg_match("/^([0-9:hPMA]+)\s*(?:à|to|-)\s*([0-9:hPMA]+)$/", $time, $matches))
      {
          $ret1 = calendarActions::parseTime($matches[1]);
          $ret2 = calendarActions::parseTime($matches[2]);
          if ($ret2 == null)
          {
              return $ret1;
          }
          else if ($ret1["type"] != "hour" || $ret2["type"] != "hour")
          {
              return null;
          }
          else
          {
              $timeStart = $ret1["timeStart"];
              $timeEnd = $ret2["timeStart"];
              $hoursStart = $ret1["hoursStart"];
              $minutesStart = $ret1["minutesStart"];
              $hoursEnd = $ret2["hoursStart"];
              $minutesEnd = $ret2["minutesStart"];
              $type = "slot";
          }
          // Timeslots
          // e.g. 6:37-18:37
      }
      else
      {
          return null;
      }
      if($hoursStart>24 || $minutesStart>60 || $hoursEnd>24|| $minutesEnd>60) 
	return null;
      return array(
          "timeStart" => $timeStart,
          "timeEnd"   => $timeEnd,
          "hoursStart"     => intval($hoursStart),//change from string to int
          "minutesStart"   => intval($minutesStart),
          "hoursEnd"     => intval($hoursEnd),
          "minutesEnd"   => intval($minutesEnd),
          "type"      => $type
      );
  }
}
